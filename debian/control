Source: libanyevent-termkey-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libmodule-build-perl,
               perl
Build-Depends-Indep: libanyevent-perl <!nocheck>,
                     libterm-termkey-perl <!nocheck>
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libanyevent-termkey-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libanyevent-termkey-perl.git
Homepage: https://metacpan.org/release/AnyEvent-TermKey
Rules-Requires-Root: no

Package: libanyevent-termkey-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libanyevent-perl,
         libterm-termkey-perl
Description: module for terminal key input using libtermkey with AnyEvent
 AnyEvent::TermKey implements an asynchronous perl wrapper around the
 libtermkey library, which provides an abstract way to read keypress events in
 terminal-based programs. It yields structures that describe keys, rather than
 simply returning raw bytes as read from the TTY device.
 .
 It internally uses an instance of Term::TermKey to access the underlying C
 library. For details on general operation, including the representation of
 keypress events as objects, see the documentation on that class.
 .
 Proxy methods exist for normal accessors of Term::TermKey, and the usual
 behaviour of the getkey or other methods is instead replaced by the on_key
 event.
